# Dokoni plugin

This plugin for the [Terramenta framework](https://bitbucket.org/teamninjaneer/terramenta/wiki/Home) shows  on a 3D map, the locations of Android phones running the DokoniApp.  

Android phones running the [DokoniApp](https://bitbucket.org/workingDog/dokoniapp) send their locations information to this 
Dokoni plugin. The plugin then displays the locations on the world map, and 
as the Android devices are moved the map is automatically updated.

In technical terms: Dokoni is a plugin for the NASA Worldwind, 
 [Terramenta framework](https://bitbucket.org/teamninjaneer/terramenta/wiki/Home). 
Dokoni is a server that listens to clients (the Android devices) for locations messages.

You need to install the DokoniApp on the phones you want to track, and you need to install
and start this Dokoni plugin on your computer. The Android phones will then send their locations to
the computer so that family and friends can see where you are.

Ringo Wathelet


## How to install the Dokoni plugin

The plugin needs to be installed into Terramenta for it to work. To do that:

  - download **Terramenta** from: [Terramenta downloads](https://bitbucket.org/teamninjaneer/terramenta/downloads)
  - download the **Dokoni-xxx.nbm** plugin from: [Dokoni downloads](https://bitbucket.org/workingDog/dokoniplugin/downloads)
  - launch **Terramenta** by double clicking on the file terramenta in the /bin directory  
  - once Terramenta is running go to *Tools->Plugins->Downloaded* and click on the *Add Plugin* button. Navigate to the plugin directory and select the **Dokoni-xxx.nbm**.
  - press the Install button to install the plugin.

Once the plugin is installed, it will be present the next time you open Terramenta, until you decide to uninstall it.

## To uninstall the Dokoni plugin

To uninstall the plugin from Terramenta, launch Terramenta, go to *Tools->Plugins->Installed* and select **Dokoni** and click on the *Uninstall* button then restart Terramenta. 

## To use the Dokoni plugin

Tracking of the Android phones is started by pressing the *Monitor* button. To monitor everyone sending their locations 
to you, tick the *Monitor everyone* box. To only monitor selected contacts, deselect *Monitor everyone* and 
select the contacts from the table of names and phone numbers. 

To populate the table with names and phone numbers (contacts) you want to track, 
used the *Add* button and edit the appropriate entry. 
All monitoring is based on phone numbers. So the phone number in the table 
must match exactly the phone number you want to track.

The table of contacts can be saved using the *Save* button. A file called *contacts.txt* will be 
created/updated in your home directory.
 
The Dokoni plugin should be started before the DokoniApp on the phones are started.

## Technical 

The plugin processes [NMEA 2.0 and 2.3 0183 RMC](http://en.wikipedia.org/wiki/NMEA_0183) messages containing location information, 
plus additional user information such as name, telephone number and device id.
This plugin processes the clients [NMEA sentences](http://www.gpsinformation.org/dale/nmea.htm) from http GET requests, 
with the following key,value parameters, as shown in these examples:

  - key=NMEA_0183RMC
  - value=$GPRMC,002456,A,3553.5295,N,13938.6570,E,0.0,43.1,180700,7.1,W,A*3D

  - key=name
  - value=Wathelet

  - key=number
  - value=12345678

  - key=id
  - value=9774d56d682f549c

Once an http GET request is received from a client, it is decoded and the location is extracted.
The plugin then displays the location on the map. 

## Status

no testing using a real phone has been done yet, as I don't have an Android phone.
Using a phone emulator, I was able to display the locations on the map. 


