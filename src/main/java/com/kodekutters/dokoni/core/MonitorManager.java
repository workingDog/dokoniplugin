package com.kodekutters.dokoni.core;

import com.kodekutters.dokoni.server.HttpServer;
import com.kodekutters.dokoni.server.MessageConsumer;
import com.kodekutters.dokoni.service.ServerNetService;
import com.kodekutters.dokoni.service.ServerService;
import org.openide.util.Lookup;

/**
 * manages the server 
 * @author Ringo Wathelet, March 2013
 */
public class MonitorManager {

    private static ServerService serverService = Lookup.getDefault().lookup(ServerNetService.class);

    public MonitorManager(MessageConsumer consumer, int port) {
        if (serverService == null) {
            serverService = new ServerNetService(port);
        }
        ((HttpServer) serverService.getServer()).getHandler().addListener(consumer);
    }

    public void start() {
        serverService.start();
    }

    public void stop() {
        serverService.stop();
    }

    public void updateDuration() {
        this.getServerService().setDuration(this.getServerService().getDuration()
                + this.getServerService().getInfoTimer().getDelay() / 1000);
        this.getServerService().getInfoTimer().restart();
    }

    public ServerService getServerService() {
        return serverService;
    }
}
