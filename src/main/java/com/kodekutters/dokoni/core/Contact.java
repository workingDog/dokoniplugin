package com.kodekutters.dokoni.core;

/**
 * Contact, holds information about the phone (owner)
 *
 * @author Ringo Wathelet, March 2013
 */
public class Contact {

    private String name;
    private String phoneNumber;
    private String deviceId;  // for future use
    private boolean selected;

    public Contact(boolean selected, String name, String phoneNumber, String dev) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.selected = selected;
        this.deviceId = dev;
    }

    public Contact(boolean selected, String name, String phoneNumber) {
        this(selected, name, phoneNumber, "0");
    }

    /**
     * note: does not test the selected field
     */
    public boolean equals(Contact con) {
        return this.name.equals(con.getName())
                && this.phoneNumber.equals(con.getPhoneNumber())
                && this.deviceId.equals(con.getDeviceId());
    }

    public boolean equalOnDeviceId(Contact con) {
        return this.deviceId.equals(con.getDeviceId());
    }

    public boolean equalOnNumber(Contact con) {
        return this.phoneNumber.equals(con.getPhoneNumber());
    }

    public boolean equalOnName(Contact con) {
        return this.name.equals(con.getName());
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String toXml() {
        StringBuilder sb = new StringBuilder();
        sb.append("<contact>\n");
        sb.append("\t<selected> ").append(String.valueOf(selected)).append(" </selected>\n");
        sb.append("\t<name> ").append(name).append(" </name>\n");
        sb.append("\t<phoneNumber> ").append(phoneNumber).append(" </phoneNumber>\n");
        sb.append("\t<deviceId> ").append(deviceId).append(" </deviceId>\n");
        sb.append("</contact>\n");
        return sb.toString();
    }

    public String toString() {
        return String.valueOf(selected) + "," + name + "," + phoneNumber + "," + deviceId;
    }
}
