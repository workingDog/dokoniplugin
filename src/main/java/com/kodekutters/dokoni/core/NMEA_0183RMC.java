package com.kodekutters.dokoni.core;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An NMEA 2.0 to 2.3 recommended minimum specific GPS/Transit sentence
 * 
 * Author: Ringo Wathelet Date: 7/04/13 Version: 1
 */
public class NMEA_0183RMC {

    double latitude = 0.0;
    double longitude = 0.0;
    double speed = 0.0;
    double course = 0.0;
    double magVar = 0.0;
    Calendar timeDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

    // NMEA recommended minimum specific GPS/Transit data
    // references:
    // http://www.gpsinformation.org/dale/nmea.htm#RMC
    // http://aprs.gids.nl/nmea/
    // http://www.winsystems.com/software/nmea.pdf
    // http://en.wikipedia.org/wiki/NMEA_0183
    //
    // $GPRMC,065411.99,V,0724.339,S,04708.235,E,94.2,17.42,010413,,E*44
    // $GPRMC,092751.000,A,5321.6802,N,00630.3371,W,0.06,31.66,280511,,,A*45
    // $GPRMC,123519,A,4807.038,N,01131.000,E,022.4,084.4,230394,003.1,W*6A
    // $GPRMC,002456,A,3553.5295,N,13938.6570,E,0.0,43.1,180700,7.1,W,A*3D
    //
    //      $            sentence starts with a dollar sign
    //      RMC          Recommended Minimum sentence C, prepended with GP for GPS or GL for GLONASS
    //      123519.12    Fix taken at 12:35:19:12 UTC, optional with milisecs
    //      A            Status A=active or V=Void.
    //      4807.038,N   Latitude 48 deg 07.038' N
    //      01131.000,E  Longitude 11 deg 31.000' E
    //      022.4        Speed over the ground in knots
    //      084.4        Track angle in degrees True
    //      230394       Date - 23rd of March 1994
    //      003.1,W      Magnetic Variation
    //      A            NMEA 2.3 FAA Position System Mode indicator character, A=autonomous, D=differential, E=Estimated, N=not valid, S=Simulator
    //      *6A          The checksum data, always begins with *
    //      \crt\lf      sentence suppose to end with control line feed
    //
    // ([^\$]*) match everything until (excluding) the first dollar sign, used to skip anything before the NMEA sentence
    // ((?s).*) match everything including new lines, used to skip everything after the NMEA sentence
    //
    public NMEA_0183RMC(final String sentence) {

        Pattern pattern = Pattern.compile("([^\\$]*)(\\$GPRMC),(\\d+\\.?\\d+?),([AV]),"
                + "(\\d{2})(\\d{2}\\.?\\d+?),([NS]),(\\d{3})(\\d{2}\\.?\\d+?),([EW]),"
                + "(\\d+\\.\\d+)?,(\\d+\\.\\d+)?,(\\d{2})?(\\d{2})?(\\d{2})?,(\\d+?\\.\\d+)?,([EW])?,?"
                + "([ADENS])?\\*([a-zA-Z0-9]+)((?s).*)");

        Matcher matcher = pattern.matcher(sentence);

        if (matcher.matches()) {
            matcher.reset();
            if (matcher.find()) {
                int startNdx = sentence.indexOf("$") + 1;
                int endNdx = sentence.indexOf("*");

                if (isValidChecksum(sentence.substring(startNdx, endNdx), matcher.group(19))) {
                    String latDeg = "0";
                    if (matcher.group(5) != null) {
                        latDeg = matcher.group(5);
                    }
                    String latMin = "0";
                    if (matcher.group(6) != null) {
                        latMin = matcher.group(6);
                    }
                    String latHemis = "N";
                    if (matcher.group(7) != null) {
                        latHemis = matcher.group(7);
                    }                   
                    latitude = Double.valueOf(latDeg) + (Double.valueOf(latMin) / 60.0);
                    if (latHemis.equalsIgnoreCase("S")) {
                        latitude = -latitude;
                    }
                    
                    String lonDeg = "0";
                    if (matcher.group(8) != null) {
                        lonDeg = matcher.group(8);
                    }
                    String lonMin = "0";
                    if (matcher.group(9) != null) {
                        lonMin = matcher.group(9);
                    }
                    String lonHemis = "N";
                    if (matcher.group(10) != null) {
                        lonHemis = matcher.group(10);
                    }                    
                    longitude = Double.valueOf(lonDeg) + (Double.valueOf(lonMin) / 60.0);
                    if (lonHemis.equalsIgnoreCase("W")) {
                        longitude = -longitude;
                    }

                    if (matcher.group(11) != null) {
                        speed = Double.valueOf(matcher.group(11));
                    }
                    if (matcher.group(12) != null) {
                        course = Double.valueOf(matcher.group(12));
                    }
                    if (matcher.group(16) != null) {
                        magVar = Double.valueOf(matcher.group(16));
                    }
                    if (matcher.group(17) != null) {
                        if (matcher.group(17).equalsIgnoreCase("W")) {
                            magVar = -magVar;
                        }
                    }

                    if (matcher.group(3) != null) {
                        timeDate.clear();
                        String t = matcher.group(3);
                        timeDate.set(Calendar.HOUR, Integer.parseInt(t.substring(0, 2)));
                        timeDate.set(Calendar.MINUTE, Integer.parseInt(t.substring(2, 4)));
                        timeDate.set(Calendar.SECOND, Integer.parseInt(t.substring(4, 6)));
                    }

                    int day = 0;
                    if (matcher.group(13) != null) {
                        day = Integer.parseInt(matcher.group(13));
                    }
                    int month = 0;
                    if (matcher.group(14) != null) {
                        month = Integer.parseInt(matcher.group(14));
                    }
                    int year = 0;
                    if (matcher.group(15) != null) {
                        year = Integer.parseInt(matcher.group(15));
                    }

                    timeDate.set(Calendar.DAY_OF_MONTH, day);
                    timeDate.set(Calendar.MONTH, month);
                    timeDate.set(Calendar.YEAR, 2000 + year);
                }
            }
        }
    }

    @Override
    public String toString() {
        return getLatitude() + ", " + getLongitude() + ", "
                + new SimpleDateFormat("yyMMddHHmmssZ").format(timeDate.getTime())
                + ", " + speed + ", " + course + ", " + magVar;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getAltitude() {
        return 0.0;
    }

    public Calendar getTimeDate() {
        return timeDate;
    }

    public double getSpeed() {
        return speed;
    }

    public double getCourse() {
        return course;
    }

    public double getMagVar() {
        return magVar;
    }

    private boolean isValidChecksum(String nmea, String required) {
        int sum = 0;
        for (int i = 0; i < nmea.length(); i++) {
            sum ^= (byte) nmea.charAt(i);
        }
        return required.equals(String.format("%02X", sum));
    }
}
