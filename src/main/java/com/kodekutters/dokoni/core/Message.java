package com.kodekutters.dokoni.core;

import gov.nasa.worldwind.geom.Position;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.eclipse.jetty.server.Request;

/**
 * Message constructed from information (Request) received by the server
 *
 * @author Ringo Wathelet, March 2013
 */
public class Message {

    public enum MessageType {

        LOCATION, NOT_INTERESTED
    }
    private MessageType messageType = MessageType.NOT_INTERESTED;
    private Contact contact = null;
    private NMEA_0183RMC sentence = null;

    public Message() {
    }

    public String getNmeaSentence() {
        return sentence.toString();
    }

    public double getLatitude() {
        return sentence.getLatitude();
    }

    public double getLongitude() {
        return sentence.getLongitude();
    }

    public double getAltitude() {
        return sentence.getAltitude();
    }

    public Position getPosition() {
        return Position.fromDegrees(getLatitude(), getLongitude(), getAltitude());
    }

    public Message(Request baseRequest) {
        if (baseRequest.getMethod().equals("GET")) {
            try {
                sentence = new NMEA_0183RMC(baseRequest.getParameter("NMEA_0183RMC").trim());

                // must have a valid position
                if (isValidLatDeg(sentence.getLatitude()) && isValidLonDeg(sentence.getLongitude())) {
                    String phoneNumber = baseRequest.getParameter("number").trim();
                    // must have a potentially valid phone number
                    if ((phoneNumber == null) || phoneNumber.isEmpty()) {
                        messageType = MessageType.NOT_INTERESTED;
                        contact = null;
                    } else {
                        messageType = MessageType.LOCATION;
                        contact = new Contact(false, baseRequest.getParameter("name").trim(),
                                phoneNumber, baseRequest.getParameter("id").trim());
                    }
                } else {
                    messageType = MessageType.NOT_INTERESTED;
                    contact = null;
                }
            } catch (Exception ex) {
                System.out.println("----ERROR in message Request: " + baseRequest.toString() + " " + ex);
                messageType = MessageType.NOT_INTERESTED;
                contact = null;
            }
        } else {
            messageType = MessageType.NOT_INTERESTED;
            contact = null;
        }
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public Contact getContact() {
        return contact;
    }

    @Override
    public String toString() {
        return sentence.toString();
    }

    public Calendar getTimestamp() {
        return sentence.timeDate;
    }

    public String getTimeAsSimpleDateFormat() {
        return new SimpleDateFormat("yyMMddHHmmssZ").format(sentence.timeDate.getTime());
    }

    public Long getTimeAsMilliSec() {
        return sentence.timeDate.getTimeInMillis();
    }

    public static boolean isValidLatDeg(double val) {
        return (val >= -90 && val <= 90);
    }

    public static boolean isValidLonDeg(double val) {
        return (val >= -180 && val <= 180);
    }
}
