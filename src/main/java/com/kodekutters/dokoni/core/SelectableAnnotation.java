package com.kodekutters.dokoni.core;

import com.kodekutters.dokoni.layer.EntityType;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.AnnotationAttributes;
import gov.nasa.worldwind.render.GlobeAnnotation;
import java.awt.Color;
import java.awt.Insets;
import java.awt.Point;
import java.util.UUID;

/**
 * SelectableAnnotation
 *
 * A GlobeAnnotation that is "linked" to a selectable object, such that you can
 * select the annotation to select the object.
 *
 * @author Ringo Wathelet
 * @version 1.0  March, 2012
 */
public class SelectableAnnotation extends GlobeAnnotation implements EntityType {

    private EntityType selectableObject;
    private String name = "annotation";
    private Color originalBorderColor = Color.black;
    private Color selectedBorderColor = Color.yellow;
    private double originalBorderWidth = 2;
    private double selectedBorderWidth = 3;

    public SelectableAnnotation(String text, Position position) {
        this(text, position, null);
    }

    public SelectableAnnotation(String text, Position position, EntityType selectableObject) {
        super(text, position);
        this.setAttributes(getDefaultAttributes());
        this.selectableObject = selectableObject;
    }

    public EntityType getSelectableObject() {
        return selectableObject;
    }

    public void setSelectableObject(EntityType selectableObject) {
        this.selectableObject = selectableObject;
    }

    @Override
    public boolean isSelected() {
        return selectableObject.isSelected();
    }

    @Override
    public void setSelected(boolean selected) {
        setSelected(selected, false);
    }

    // beware of cyclic selections here
    public void setSelected(boolean selected, boolean annotationOnly) {
        if (!annotationOnly) {
            selectableObject.setSelected(selected);
        }
        if (selected) {
            this.getAttributes().setBorderColor(selectedBorderColor);
            this.getAttributes().setBorderWidth(selectedBorderWidth);
        } else {
            this.getAttributes().setBorderColor(originalBorderColor);
            this.getAttributes().setBorderWidth(originalBorderWidth);
        }
    }

    private AnnotationAttributes getDefaultAttributes() {
        AnnotationAttributes theAttributes = new AnnotationAttributes();
        theAttributes.setCornerRadius(10);
        theAttributes.setInsets(new Insets(8, 8, 8, 8));
        theAttributes.setBackgroundColor(new Color(0f, 0f, 0f, .5f));
        theAttributes.setTextColor(Color.WHITE);
        theAttributes.setDistanceMinScale(.5);
        theAttributes.setDistanceMaxScale(2);
        theAttributes.setDistanceMinOpacity(.5);
        theAttributes.setLeaderGapWidth(14);
        theAttributes.setDrawOffset(new Point(20, 40));
        originalBorderWidth = theAttributes.getBorderWidth();
        originalBorderColor = theAttributes.getBorderColor();
        selectedBorderWidth = originalBorderWidth * 2;
        return theAttributes;
    }

    public Color getOriginalBorderColor() {
        return originalBorderColor;
    }

    public void setOriginalBorderColor(Color originalBorderColor) {
        this.originalBorderColor = originalBorderColor;
    }

    public double getOriginalBorderWidth() {
        return originalBorderWidth;
    }

    public void setOriginalBorderWidth(double originalBorderWidth) {
        this.originalBorderWidth = originalBorderWidth;
    }

    public Color getSelectedBorderColor() {
        return selectedBorderColor;
    }

    public void setSelectedBorderColor(Color selectedBorderColor) {
        this.selectedBorderColor = selectedBorderColor;
    }

    public double getSelectedBorderWidth() {
        return selectedBorderWidth;
    }

    public void setSelectedBorderWidth(double selectedBorderWidth) {
        this.selectedBorderWidth = selectedBorderWidth;
    }

    @Override
    public UUID getId() {
        return selectableObject.getId();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String val) {
        this.name = val;
    }
}
