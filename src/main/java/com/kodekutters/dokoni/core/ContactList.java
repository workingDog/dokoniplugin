package com.kodekutters.dokoni.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.openide.util.Exceptions;

/**
 * ContactList, the master list of contacts
 *
 * @author Ringo Wathelet
 * @version 1.0, 2013
 */
public enum ContactList {

    INSTANCE;
    private final EventListenerList listeners = new EventListenerList();
    private final CopyOnWriteArrayList<Contact> contactList = new CopyOnWriteArrayList();
    private final String contactsFileName = "contacts.txt";
    private final String NL = System.getProperty("line.separator");

    {
        readContactsJ16();
    }

    /**
     * read the contacts from file, Java 1.6
     */
    private void readContactsJ16() {
        List<String> listOfLines = new ArrayList();
        File theContactsFile = new File(contactsFileName);
        if ((theContactsFile != null) && theContactsFile.exists()) {
            try {
                Scanner scanner = new Scanner(new FileInputStream(contactsFileName));
                try {
                    while (scanner.hasNextLine()) {
                        listOfLines.add(scanner.nextLine() + NL);
                    }
                } finally {
                    scanner.close();
                }
            } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
            }
            for (String line : listOfLines) {
                String[] part = line.split(",");
                if (part.length >= 4) {
                    contactList.add(new Contact(Boolean.parseBoolean(part[0].trim()), part[1].trim(), part[2].trim(), part[3].trim()));
                }
            }
        }
    }

    /*
     * save the contacts to file, Java 1.6
     */
    public void saveContacts() {
        File theContactsFile = new File(contactsFileName);
        if ((theContactsFile != null) && theContactsFile.exists()) {
            // clear the file
            try {
                PrintWriter writer = new PrintWriter(new File(contactsFileName));
                writer.print("");
                writer.close();
            } catch (FileNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

        Writer out = null;
        try {
            out = new OutputStreamWriter(new FileOutputStream(contactsFileName));
            for (Contact contact : contactList) {
                out.write(contact.toString() + NL);
            }
            out.close();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    /**
     * read the contacts from file, Java 1.7
     */
//    private void readContactsJ17() {
//        List<String> listOfLines = new ArrayList();
//        try {
//            listOfLines = Files.readAllLines(Paths.get(contactsFileName), StandardCharsets.UTF_8);
//        } catch (IOException ex) {
//            Exceptions.printStackTrace(ex);
//        }
//        for (String line : listOfLines) {
//            String[] part = line.split(",");
//            if (part.length >= 4) {
//                contactList.add(new Contact(Boolean.parseBoolean(part[0].trim()), part[1].trim(), part[2].trim(), part[3].trim()));
//            }
//        }
//    }

    /*
     * save the contacts to file, Java 1.7
     */
//    public void saveContacts() {
//        // clear the file first
//        try {
//            PrintWriter writer = new PrintWriter(new File(contactsFileName));
//            writer.print("");
//            writer.close();
//        } catch (FileNotFoundException ex) {
//            Exceptions.printStackTrace(ex);
//        }
//        // now copy the contacts into an array
//        List<String> listOfLines = new ArrayList();
//        for (Contact contact : contactList) {
//            listOfLines.add(contact.toString());
//        }
//        // write to file
//        try {
//            Files.write(Paths.get(contactsFileName), listOfLines, StandardCharsets.UTF_8);
//        } catch (IOException ex) {
//            Exceptions.printStackTrace(ex);
//        }
//    }
    public CopyOnWriteArrayList<Contact> getContacts() {
        return contactList;
    }

    public boolean contains(Contact contact) {
        return contactList.contains(contact);
    }

    public Contact getOnNumber(Contact con) {
        for (Contact contact : contactList) {
            if (contact.equalOnNumber(con)) {
                return contact;
            }
        }
        return null;
    }

    public void add(Contact contact) {
        contactList.add(contact);
        fireContactAddedEvent(contact);
    }

    public void remove(Contact contact) {
        contactList.remove(contact);
        fireContactRemovedEvent(contact);
    }

    /**
     * remove a contact given the phone number
     *
     * @param name
     * @param number
     */
    public void remove(String number) {
        for (Contact contact : contactList) {
            // test number only
            if (contact.equalOnNumber(new Contact(false, "", number.trim()))) {
                contactList.remove(contact); // can do this because CopyOnWriteArrayList 
            }
        }
    }

    public void addEventListener(ListDataListener listener) {
        listeners.add(ListDataListener.class, listener);
    }

    public void removeEventListener(ListDataListener listener) {
        listeners.remove(ListDataListener.class, listener);
    }

    public final void fireContactsChangedEvent() {
        for (ListDataListener listener : listeners.getListeners(ListDataListener.class)) {
            listener.contentsChanged(
                    new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, 0));
        }
    }

    protected final void fireContactAddedEvent(Contact contact) {
        for (ListDataListener listener : listeners.getListeners(ListDataListener.class)) {
            listener.contentsChanged(
                    new ListDataEvent(contact, ListDataEvent.INTERVAL_ADDED, 0, 0));
        }
    }

    protected final void fireContactRemovedEvent(Contact contact) {
        for (ListDataListener listener : listeners.getListeners(ListDataListener.class)) {
            listener.contentsChanged(
                    new ListDataEvent(contact, ListDataEvent.INTERVAL_REMOVED, 0, 0));
        }
    }
}
