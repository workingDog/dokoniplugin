package com.kodekutters.dokoni.service;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.event.EventListenerList;
import org.openide.util.Lookup;

/**
 * ServerService
 *
 * @author Ringo Wathelet
 * @version 1.0, 2013
 */
public abstract class ServerService implements ActionListener {

    private final EventListenerList listeners = new EventListenerList();
    private int duration = 0;
    private Timer infoTimer = new Timer(1000, null);

    public static ServerService getDefault() {
        ServerService service = Lookup.getDefault().lookup(ServerService.class);
        if (service == null) {
            service = new DefaultServerService();
        }

        return service;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Timer getInfoTimer() {
        return infoTimer;
    }

    public void setInfoTimer(Timer infoTimer) {
        this.infoTimer = infoTimer;
    }

    public void addEventListener(ServerEventListener listener) {
        listeners.add(ServerEventListener.class, listener);
    }

    public void removeEventListener(ServerEventListener listener) {
        listeners.remove(ServerEventListener.class, listener);
    }

    protected final void fireStartEvent() {
        for (ServerEventListener listener : listeners.getListeners(ServerEventListener.class)) {
            listener.starting();
        }
    }

    protected final void fireStopEvent() {
        for (ServerEventListener listener : listeners.getListeners(ServerEventListener.class)) {
            listener.stopping();
        }
    }

    public abstract Object getServer();

    public abstract void setServer(Object handler);

    public abstract void start();

    public abstract void stop();

    public abstract boolean isRunning();

    public abstract Object getHandler();

    @Override
    public void actionPerformed(ActionEvent ae) {
        this.setDuration(this.getDuration() + this.getInfoTimer().getDelay() / 1000);
        this.getInfoTimer().restart();
    }

    private static class DefaultServerService extends ServerService {

        @Override
        public void start() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void stop() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public int getDuration() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setDuration(int d) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Timer getInfoTimer() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setInfoTimer(Timer infoTimer) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Object getServer() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setServer(Object handler) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean isRunning() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Object getHandler() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
