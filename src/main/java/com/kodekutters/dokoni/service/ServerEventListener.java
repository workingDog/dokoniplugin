package com.kodekutters.dokoni.service;

import java.util.EventListener;

/**
 * ServerServiceEventListener
 *
 * @author Ringo Wathelet
 * @version 1.0, 2013
 */
public interface ServerEventListener extends EventListener {

    public void starting();
    
    public void restarting();

    public void stopping();

    public void pausing();

    public void autoStarting();

    public void autoStopping();
}
