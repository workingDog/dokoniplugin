package com.kodekutters.dokoni.service;

import com.kodekutters.dokoni.server.HttpServer;
import com.kodekutters.dokoni.server.PhoneRequestHandler;
import org.openide.util.lookup.ServiceProvider;

/**
 * ServerNetService an implementation of the abstract ServerService.
 * Provides an http server
 *
 * @author R. Wathelet
 * @version 1.0
 */
@ServiceProvider(service = ServerService.class)
public class ServerNetService extends ServerService {

    protected HttpServer server;

    public ServerNetService() {
        server = new HttpServer();
    }

    public ServerNetService(int port) {
        server = new HttpServer(port);
    }

    @Override
    public HttpServer getServer() {
        return server;
    }

    @Override
    public void setServer(Object serv) {
        this.server = (HttpServer) serv;
    }

    @Override
    public PhoneRequestHandler getHandler() {
        return this.server.getHandler();
    }

    @Override
    public void start() {
        fireStartEvent();
        this.setDuration(0);
        this.getInfoTimer().start();
        if (!this.isRunning()) {
            Thread thread = new Thread("http server thread") {
                public void run() {
                    getServer().start();
                }
            };
            thread.start();
        }
    }

    @Override
    public void stop() {
        fireStopEvent();
        this.getInfoTimer().stop();
        if (this.isRunning()) {
            this.getServer().stop();
        }
    }

    @Override
    public boolean isRunning() {
        return this.getServer().isRunning();
    }
}
