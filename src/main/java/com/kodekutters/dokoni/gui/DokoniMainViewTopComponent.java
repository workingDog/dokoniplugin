package com.kodekutters.dokoni.gui;

import com.kodekutters.dokoni.core.AnnotationDragger;
import com.kodekutters.dokoni.core.Contact;
import com.kodekutters.dokoni.core.ContactList;
import com.kodekutters.dokoni.core.Message;
import com.kodekutters.dokoni.core.MonitorManager;
import com.kodekutters.dokoni.layer.DokoniLayer;
import com.kodekutters.dokoni.server.HttpServer;
import com.kodekutters.dokoni.server.MessageConsumer;
import com.terramenta.globe.WorldWindManager;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;

/**
 * main interface to control the incoming client location messages
 *
 *
 * @author Ringo Wathelet, March 2013
 */
@ConvertAsProperties(
        dtd = "-//com.kodekutters.dokoni.gui//DokoniMainView//EN",
        autostore = false)
@TopComponent.Description(
        preferredID = "DokoniMainViewTopComponent",
        iconBase = "com/kodekutters/image/resources/signal.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "rightSide", openAtStartup = true)
@ActionID(category = "Window", id = "com.kodekutters.dokoni.gui.DokoniMainViewTopComponent")
@ActionReference(path = "Menu/Window" /*, position = 333 */)
@TopComponent.OpenActionRegistration(
        displayName = "#CTL_DokoniMainViewAction",
        preferredID = "DokoniMainViewTopComponent")
@Messages({
    "CTL_DokoniMainViewAction=Dokoni",
    "CTL_DokoniMainViewTopComponent=Dokoni",
    "HINT_DokoniMainViewTopComponent=Dokoni Window"
})
public final class DokoniMainViewTopComponent extends TopComponent implements MessageConsumer {

    private static final WorldWindManager wwm = Lookup.getDefault().lookup(WorldWindManager.class);
    private DokoniLayer dokoniLayer = null;
    private MonitorManager monitorManager = null;
    private int port = HttpServer.port;
    protected Color portFieldBackground;
    public static final String DOKONI_LAYER = "Dokoni";

    public DokoniMainViewTopComponent() {
        initComponents();
        portFieldBackground = portField.getBackground();

        setName(Bundle.CTL_DokoniMainViewTopComponent());
        setToolTipText(Bundle.HINT_DokoniMainViewTopComponent());

        dokoniLayer = (DokoniLayer) wwm.getWorldWindow().getModel().getLayers().getLayerByName(DOKONI_LAYER);
        if (dokoniLayer == null) {
            dokoniLayer = new DokoniLayer(DOKONI_LAYER);
            wwm.getLayers().add(dokoniLayer);
            wwm.getLayers().add(dokoniLayer.getAnnotationLayer());
            wwm.getLayers().add(dokoniLayer.getHistoryLayer());
        }

        wwm.getWorldWindow().addSelectListener(new AnnotationDragger(wwm.getWorldWindow()));

        // to shutdown the server when closing the window
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                if (monitorManager != null) {
                    if (monitorManager.getServerService().isRunning()) {
                        try {
                            ((HttpServer) monitorManager.getServerService().getServer()).stop();
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                }
            }
        }, "Stop Server"));

        // fill the ip and host info labels
        try {
            InetAddress ip = InetAddress.getLocalHost();
            ipTextLabel.setText(getMyExternalIpAddress());
            hostTextLabel.setText(ip.getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        populateContactTable();

        // make the dokoniLayer listen to changes in the ContactList
        ((DefaultTableModel) contactTable.getModel()).addTableModelListener(dokoniLayer);

        // let the layer know when the checkboxes change
        monitorAllCheckbox.addChangeListener(dokoniLayer);
        historyCheckBox.addChangeListener(dokoniLayer);

        // as default, monitor everyone
        monitorAllCheckbox.setSelected(true);
    }

    /**
     * get my IP address as seen by an external application. Use an external
     * public server to get this.
     *
     * @return the global IP address
     */
    private String getMyExternalIpAddress() {
        String ip = null;
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
            ip = in.readLine();
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ip;
    }

    /*
     * save the contacts to file. Note the deviceId is not saved, value set to 0 for now
     */
    private void saveContacts() {
        ContactList.INSTANCE.getContacts().clear();
        TableModel tableModel = ((DefaultTableModel) contactTable.getModel());
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            boolean selected = (Boolean) tableModel.getValueAt(i, 0);
            String name = (String) tableModel.getValueAt(i, 1);
            String number = (String) tableModel.getValueAt(i, 2);
            ContactList.INSTANCE.getContacts().add(new Contact(selected, name.trim(), number.trim()));
        }
        ContactList.INSTANCE.saveContacts();
    }

    private void populateContactTable() {
        ((DefaultTableModel) contactTable.getModel()).setRowCount(0);  // erases the table 
        for (Contact contact : ContactList.INSTANCE.getContacts()) {
            Object[] aRow = {contact.isSelected(), contact.getName(), contact.getPhoneNumber()};
            ((DefaultTableModel) contactTable.getModel()).addRow(aRow);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        backPanel = new javax.swing.JPanel();
        listScrollPane = new javax.swing.JScrollPane();
        contactTable = new javax.swing.JTable();
        monitorButton = new javax.swing.JToggleButton();
        progressBar = new javax.swing.JProgressBar();
        textScrollPane = new javax.swing.JScrollPane();
        textArea = new javax.swing.JTextArea();
        portLabel = new javax.swing.JLabel();
        historyCheckBox = new javax.swing.JCheckBox();
        timeLabel = new javax.swing.JLabel();
        timeUnitLabel = new javax.swing.JLabel();
        portField = new javax.swing.JFormattedTextField();
        ipTextLabel = new javax.swing.JLabel();
        ipLabel = new javax.swing.JLabel();
        hostLabel = new javax.swing.JLabel();
        hostTextLabel = new javax.swing.JLabel();
        addButton = new javax.swing.JButton();
        removeButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        monitorAllCheckbox = new javax.swing.JCheckBox();

        contactTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Show", "Name", "Phone number"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        listScrollPane.setViewportView(contactTable);
        contactTable.getColumnModel().getColumn(0).setMinWidth(30);
        contactTable.getColumnModel().getColumn(0).setPreferredWidth(45);
        contactTable.getColumnModel().getColumn(0).setMaxWidth(60);
        contactTable.getColumnModel().getColumn(0).setHeaderValue(org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.contactTable.columnModel.title0")); // NOI18N
        contactTable.getColumnModel().getColumn(1).setHeaderValue(org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.contactTable.columnModel.title1")); // NOI18N
        contactTable.getColumnModel().getColumn(2).setHeaderValue(org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.contactTable.columnModel.title2")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(monitorButton, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.monitorButton.text")); // NOI18N
        monitorButton.setToolTipText(org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.monitorButton.toolTipText")); // NOI18N
        monitorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                monitorButtonActionPerformed(evt);
            }
        });

        progressBar.setToolTipText(org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.progressBar.toolTipText")); // NOI18N

        textArea.setColumns(20);
        textArea.setRows(5);
        textScrollPane.setViewportView(textArea);

        org.openide.awt.Mnemonics.setLocalizedText(portLabel, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.portLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(historyCheckBox, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.showHistory.text")); // NOI18N
        historyCheckBox.setName("showHistory"); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(timeLabel, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.timeLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(timeUnitLabel, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.timeUnitLabel.text")); // NOI18N

        portField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        portField.setText(org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.portField.text")); // NOI18N
        portField.setToolTipText(org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.portField.toolTipText")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(ipTextLabel, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.ipTextLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(ipLabel, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.ipLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(hostLabel, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.hostLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(hostTextLabel, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.hostTextLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(addButton, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.addButton.text")); // NOI18N
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(removeButton, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.removeButton.text")); // NOI18N
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(saveButton, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.saveButton.text")); // NOI18N
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(monitorAllCheckbox, org.openide.util.NbBundle.getMessage(DokoniMainViewTopComponent.class, "DokoniMainViewTopComponent.monitorAll.text")); // NOI18N
        monitorAllCheckbox.setName("monitorAll"); // NOI18N

        javax.swing.GroupLayout backPanelLayout = new javax.swing.GroupLayout(backPanel);
        backPanel.setLayout(backPanelLayout);
        backPanelLayout.setHorizontalGroup(
            backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backPanelLayout.createSequentialGroup()
                .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(backPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(listScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(textScrollPane)
                            .addGroup(backPanelLayout.createSequentialGroup()
                                .addComponent(hostLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(hostTextLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(backPanelLayout.createSequentialGroup()
                                .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(backPanelLayout.createSequentialGroup()
                                        .addComponent(ipLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ipTextLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(38, 38, 38)
                                        .addComponent(portLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(portField, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(monitorAllCheckbox))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, backPanelLayout.createSequentialGroup()
                        .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(backPanelLayout.createSequentialGroup()
                                .addComponent(addButton)
                                .addGap(33, 33, 33)
                                .addComponent(removeButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                                .addComponent(saveButton))
                            .addGroup(backPanelLayout.createSequentialGroup()
                                .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(monitorButton, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(backPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(historyCheckBox)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(backPanelLayout.createSequentialGroup()
                                        .addComponent(timeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(timeUnitLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                        .addGap(13, 13, 13)))
                .addContainerGap())
        );
        backPanelLayout.setVerticalGroup(
            backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backPanelLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hostLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hostTextLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ipLabel)
                    .addComponent(ipTextLabel)
                    .addComponent(portLabel)
                    .addComponent(portField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addComponent(listScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addButton)
                    .addComponent(removeButton)
                    .addComponent(saveButton))
                .addGap(12, 12, 12)
                .addComponent(monitorAllCheckbox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(monitorButton, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(backPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(timeLabel)
                        .addComponent(timeUnitLabel))
                    .addComponent(historyCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(backPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(backPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    /**
     * try to validate the port number, only checks port range (1 to 65535).
     *
     * @return true if the port number is valid else false
     */
    private boolean isValidPort() {
        try {
            port = Integer.valueOf(portField.getText()).intValue();
            // choose something sensible here
            if (port > 65535 || port < 1024) {
                portField.setBackground(Color.pink);
                return false;
            } else {
                portField.setBackground(portFieldBackground);
                return true;
            }
        } catch (Exception e) {
            portField.setBackground(Color.pink);
            return false;
        }
    }

    /**
     * start monitoring the network by starting the server
     */
    private void startMonitoring() {
        if (isValidPort()) {
            // this will make dokoniLayer listen to the server messages
            monitorManager = new MonitorManager(dokoniLayer, port);
            // also listen to server receiving messages here, see receiveMessage
            ((HttpServer) monitorManager.getServerService().getServer()).getHandler().addListener(this);

            // listen to the info timer events
            ActionListener timerEventListener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    monitorManager.updateDuration();
                    showDurationAsString(monitorManager.getServerService().getDuration());
                }
            };
            // give the ServerService a new info timer, and we will listen for it here
            monitorManager.getServerService().setInfoTimer(new Timer(1000, timerEventListener));

            portField.setEnabled(false);
            progressBar.setIndeterminate(true);
            showDurationAsString(0);
            monitorManager.start();
        } else {
            textArea.append("server NOT started, try again on a different port \n");
            monitorButton.setSelected(false);
            monitorButton.setEnabled(true);
        }
    }

    private void stopMonitoring() {
        portField.setEnabled(true);
        progressBar.setIndeterminate(false);
        if (monitorManager != null) {
            monitorManager.stop();
        }
    }
    private void monitorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_monitorButtonActionPerformed
        if (monitorButton.isSelected()) {
            startMonitoring();
        } else {
            stopMonitoring();
        }
    }//GEN-LAST:event_monitorButtonActionPerformed

    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
        for (int i = 0; i < contactTable.getSelectedRows().length; i++) {
            int j = contactTable.getSelectedRows()[i];
            ((DefaultTableModel) contactTable.getModel()).removeRow(j);
        }
    }//GEN-LAST:event_removeButtonActionPerformed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        Object[] aRow = {false, "no name", "no number"};
        ((DefaultTableModel) contactTable.getModel()).addRow(aRow);
    }//GEN-LAST:event_addButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        saveContacts();
    }//GEN-LAST:event_saveButtonActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JPanel backPanel;
    private javax.swing.JTable contactTable;
    private javax.swing.JCheckBox historyCheckBox;
    private javax.swing.JLabel hostLabel;
    private javax.swing.JLabel hostTextLabel;
    private javax.swing.JLabel ipLabel;
    private javax.swing.JLabel ipTextLabel;
    private javax.swing.JScrollPane listScrollPane;
    private javax.swing.JCheckBox monitorAllCheckbox;
    private javax.swing.JToggleButton monitorButton;
    private javax.swing.JFormattedTextField portField;
    private javax.swing.JLabel portLabel;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JButton removeButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JTextArea textArea;
    private javax.swing.JScrollPane textScrollPane;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JLabel timeUnitLabel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        stopMonitoring();
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    @Override
    public void receiveMessage(Message message) {
        textArea.append(" " + message.getPosition()
                + ": " + message.getContact().getName()
                + ": " + message.getContact().getPhoneNumber()
                + ": " + message.getContact().getDeviceId() + "\n");
    }

    protected void showDurationAsString(double duration) {
        if (duration < 60) {
            timeUnitLabel.setText("seconds");
        } else if ((duration >= 60) && (duration < 3600)) {
            duration = duration / 60;
            timeUnitLabel.setText("minutes");
        } else if ((duration >= 3600)) {
            duration = duration / 3600;
            timeUnitLabel.setText("hours");
        }
        timeLabel.setText(String.valueOf(new DecimalFormat("#.#").format(duration)));
    }
}
