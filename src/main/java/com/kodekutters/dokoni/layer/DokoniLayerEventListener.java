
package com.kodekutters.dokoni.layer;

import java.util.EventListener;

/**
 * DokoniLayerEventListener
 *
 * @author Ringo Wathelet
 * @version 1.0, 2013
 */
public interface DokoniLayerEventListener extends EventListener {

    public void addEntity(Entity entity);
    
    public void removeEntity(Entity entity);

    public void resetLayer();

}