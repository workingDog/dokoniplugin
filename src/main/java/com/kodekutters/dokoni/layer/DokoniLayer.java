package com.kodekutters.dokoni.layer;

import com.kodekutters.dokoni.core.Contact;
import com.kodekutters.dokoni.core.Message;
import com.kodekutters.dokoni.server.MessageConsumer;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.layers.AnnotationLayer;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Renderable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

/**
 * the layer of entities. Also creates the associated annotation and history
 * layers.
 *
 * @author Ringo Wathelet, March 2013
 */
public class DokoniLayer extends RenderableLayer implements MessageConsumer, TableModelListener, ChangeListener {

    private AnnotationLayer anoLayer = new AnnotationLayer();
    private RenderableLayer historyLayer = new RenderableLayer();
    private final ArrayList<DokoniLayerEventListener> listeners = new ArrayList();
    private boolean monitorAll = true;
    private boolean isShowHistory = false;
    private DefaultTableModel tableModel = null;

    public DokoniLayer(String name) {
        super();
        this.setName(name);
        anoLayer.setPickEnabled(true);
        historyLayer.setPickEnabled(false);
    }

    public void setName(String name) {
        super.setName(name);
        if (getAnnotationLayer() != null) {
            getAnnotationLayer().setName(name + " annotations");
        }
        if (getHistoryLayer() != null) {
            getHistoryLayer().setName(name + " history");
        }
    }

    public RenderableLayer getHistoryLayer() {
        return historyLayer;
    }

    public AnnotationLayer getAnnotationLayer() {
        return anoLayer;
    }

    public synchronized void addEventListener(DokoniLayerEventListener listener) {
        listeners.add(listener);
    }

    public synchronized void removeEventListener(DokoniLayerEventListener listener) {
        listeners.remove(listener);
    }

    /**
     * receive messages from the server
     *
     * @param message received
     */
    @Override
    public void receiveMessage(Message message) {
        try {
            switch (message.getMessageType()) {
                case LOCATION:
                    this.updateLayer(message);
                    break;

                default:
                    System.out.println("in DokoniLayer message type = " + message.getMessageType().toString());

            }
        } catch (Exception e) {
            Logger.getLogger(DokoniLayer.class.getName()).log(Level.WARNING, "Problem processing message " + e);
        }
    }

    /**
     * update the layer with new message info
     *
     * @param message
     */
    public void updateLayer(Message message) {
        if ((message.getContact() == null) || (message.getPosition() == null)) {
            return;
        }
        if (isMonitorAll()) {
            doUpdate(message);
        } else {
            if (isContactSelected(message.getContact().getPhoneNumber())) {
                doUpdate(message);
            }
        }
    }

    /**
     * do the actual layer update
     *
     * @param message
     */
    private void doUpdate(Message message) {
        // find the entity on the layer given the contact
        Entity entity = this.findEntity(message.getContact());
        // if have the entity already, update its position
        if (entity != null) {
            entity.setPosition(message.getTimeAsMilliSec(), message.getPosition());
            // update the history line if need be
            if (isShowHistory) {
                this.getHistoryLayer().removeRenderable(entity.getPolyline());
                this.getHistoryLayer().addRenderable(entity.getHistoryLine());
            }
        } else {
            // create a new entity
            this.addEntity(new Entity(message.getPosition(), message.getContact(), message.getTimeAsMilliSec()));
        }
        this.firePropertyChange(AVKey.LAYER, true, false);
    }

    /**
     * find the entity given the contact based on phone number
     *
     * @param contact to find
     * @return the entity or null
     */
    private Entity findEntity(Contact contact) {
        for (Renderable renderable : this.getRenderables()) {
            if (renderable instanceof Entity) {
                Entity entity = (Entity) renderable;
                if (entity.getContact().equalOnNumber(contact)) {
                    return entity;
                }
            }
        }
        return null;
    }

    protected final void fireRemoveEntityEvent(Entity entity) {
        for (DokoniLayerEventListener listener : listeners) {
            listener.removeEntity(entity);
        }
    }

    protected final void fireAddEntityEvent(Entity entity) {
        for (DokoniLayerEventListener listener : listeners) {
            listener.addEntity(entity);
        }
    }

    protected final void fireResetLayerEvent() {
        for (DokoniLayerEventListener listener : listeners) {
            listener.resetLayer();
        }
    }

    public void removeEntity(Entity theEntity) {
        this.renderables.remove(theEntity);
        getHistoryLayer().removeRenderable(theEntity.getPolyline());
        getAnnotationLayer().removeAnnotation(theEntity.getAnnotation());
        fireRemoveEntityEvent(theEntity);
        this.firePropertyChange(AVKey.LAYER, true, false);
    }

    public void resetLayer() {
        this.removeAllRenderables();
        getAnnotationLayer().removeAllAnnotations();
        getHistoryLayer().removeAllRenderables();
        fireResetLayerEvent();
        this.firePropertyChange(AVKey.LAYER, true, false);
    }

    public void addEntity(Entity entity) {
        this.addRenderable(entity);
        getHistoryLayer().addRenderable(entity.getHistoryLine());
        getAnnotationLayer().addAnnotation(entity.getAnnotation());
        fireAddEntityEvent(entity);
        this.firePropertyChange(AVKey.LAYER, true, false);
    }

    /**
     * adjust the entities as required to refresh the layer
     */
    public void refreshLayer() {
        // collect all entities that need to be removed, because they are not selected for display
        List<Entity> listToRemove = new ArrayList();
        for (Renderable renderable : this.getRenderables()) {
            if (renderable instanceof Entity) {
                Entity entity = (Entity) renderable;
                if (!isContactSelected(entity.getContact().getPhoneNumber())) {
                    listToRemove.add(entity);
                }
            }
        }
        // remove the list of entities that needs removing
        for (Entity entity : listToRemove) {
            this.removeEntity(entity);
        }
        this.firePropertyChange(AVKey.LAYER, true, false);
    }

    /**
     * determine if the phone number in the table of contacts, is selected
     *
     * @param pNumber the phone number to find
     * @return true if the item is selected, else false
     */
    private boolean isContactSelected(String phoneNumber) {
        for (Object entry : tableModel.getDataVector()) {
            Vector vec = (Vector) entry;
            // if table entry is selected and match the phone number
            if ((Boolean) vec.elementAt(0) && ((String) vec.elementAt(2)).equals(phoneNumber)) {
                return true;
            }
        }
        return false;
    }

    /**
     * temporary convenience method -- TODO remove this
     *
     * @param onof
     */
    public void showHistory(boolean onof) {
        this.getHistoryLayer().removeAllRenderables();
        isShowHistory = onof;
        if (isShowHistory) {
            for (Renderable renderable : this.getRenderables()) {
                if (renderable instanceof Entity) {
                    this.getHistoryLayer().addRenderable(((Entity) renderable).getHistoryLine());
                }
            }
        }
        this.firePropertyChange(AVKey.LAYER, true, false);
    }

    public boolean isMonitorAll() {
        return monitorAll;
    }

    public void setMonitorAll(boolean monitorAll) {
        this.monitorAll = monitorAll;
    }

    @Override
    /**
     * listen to the DokoniMainViewTopComponent checkboxes
     */
    public void stateChanged(ChangeEvent changeEvent) {
        AbstractButton button = (AbstractButton) changeEvent.getSource();
        if (button.getName().equals("monitorAll")) {
            setMonitorAll(button.isSelected());
            return;
        }
        if (button.getName().equals("showHistory")) {
            showHistory(button.isSelected());
        }
    }

    /**
     * listen to any changes in the table of contacts
     *
     * @param tme the event
     */
    @Override
    public void tableChanged(TableModelEvent tme) {
        if (tme.getSource() instanceof DefaultTableModel) {
            tableModel = (DefaultTableModel) tme.getSource();
            refreshLayer();
        }
    }
}