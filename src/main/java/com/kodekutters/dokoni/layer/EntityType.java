package com.kodekutters.dokoni.layer;

import gov.nasa.worldwind.geom.Position;
import java.util.UUID;

/**
 *
 * @author ringo wathelet
 */
public interface EntityType {
    
    public UUID getId();

    public boolean isSelected();

    public void setSelected(boolean val);

    public Position getPosition();

    public void setPosition(Position pos);
    
    public String getName();
    
    public void setName(String val);

}
