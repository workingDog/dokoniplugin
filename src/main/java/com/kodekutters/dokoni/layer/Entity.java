package com.kodekutters.dokoni.layer;

import com.kodekutters.dokoni.core.Contact;
import com.kodekutters.dokoni.core.SelectableAnnotation;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.Polyline;
import java.awt.Color;
import java.util.Calendar;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

/**
 * the visual entity representing the location of a phone
 *
 *
 * @author Ringo Wathelet
 * @version 1.0, 2013
 */
public class Entity extends PointPlacemark implements EntityType {

    private boolean selected;
    private final Contact contact;
    private SelectableAnnotation annotation = null;
    private final SortedMap<Long, Position> positionHistory = new TreeMap();
    private final Polyline poly = new Polyline();

    public Entity(Position pstn, Contact contact, Long time) {
        super(pstn);
        this.contact = contact;
        annotation = new SelectableAnnotation(getDisplayInfo(), pstn, this);
        this.setValue(AVKey.DISPLAY_NAME, getDisplayInfo());
        initHistoryLine();
        addToHistory(time, pstn);
        this.setVisible(true);
    }

    private void initHistoryLine() {
        poly.setFollowTerrain(true);
        poly.setPathType(AVKey.LINEAR);
        poly.setColor(Color.yellow);
        poly.setValue(AVKey.DISPLAY_NAME, getDisplayInfo());
    }

    @Override
    public UUID getId() {
        return UUID.fromString(contact.getDeviceId());
    }

    public Contact getContact() {
        return contact;
    }

    public SelectableAnnotation getAnnotation() {
        return annotation;
    }

    public void setAnnotation(SelectableAnnotation annotation) {
        this.annotation = annotation;
    }

    public String getDisplayInfo() {
        return contact.getName() + " (" + contact.getPhoneNumber() + ")";
    }

    public void setPosition(Long time, Position pos) {
        super.setPosition(pos);
        annotation.setPosition(pos);
        this.positionHistory.put(time, pos);
    }

    @Override
    public void setPosition(Position pos) {
        this.setPosition(Calendar.getInstance().getTimeInMillis(), pos);
    }

    public Polyline getHistoryLine() {
        poly.clearList();
        poly.setPositions(this.positionHistory.values());
        return poly;
    }

    public Polyline getPolyline() {
        return poly;
    }

    public SortedMap<Long, Position> getHistory() {
        return positionHistory;
    }

    public void addToHistory(Long time, Position pos) {
        this.positionHistory.put(time, pos);
    }

    @Override
    public boolean isSelected() {
        return this.selected;
    }

    @Override
    public void setSelected(boolean val) {
        this.selected = val;
    }

    @Override
    public String getName() {
        return contact.getName();
    }

    @Override
    public void setName(String val) {
        contact.setName(val);
    }
}
