package com.kodekutters.dokoni.server;

import com.kodekutters.dokoni.core.Message;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

/**
 * PhoneRequestHandler, this is where the messages from the server are handled.
 * Pass the messages to the listeners 
 *
 * @author Ringo Wathelet
 * @version 1.0, 2013
 */
public class PhoneRequestHandler extends AbstractHandler {

    protected final List<MessageConsumer> messageListeners = new ArrayList();

    public PhoneRequestHandler() {
    }

    public void addListener(Object aListener) {
        if (aListener instanceof MessageConsumer) {
            if (!(messageListeners.contains((MessageConsumer) aListener))) {
                messageListeners.add((MessageConsumer) aListener);
            }
        }
    }

    public void removeListener(Object aListener) {
        if (aListener instanceof MessageConsumer) {
            messageListeners.remove((MessageConsumer) aListener);
        }
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);

        baseRequest.setHandled(true);
        // pass the message to all listeners
        for (MessageConsumer theListener : this.messageListeners) {
            theListener.receiveMessage(new Message(baseRequest));
        }
    }
}