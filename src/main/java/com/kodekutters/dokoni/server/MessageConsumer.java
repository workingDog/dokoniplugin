package com.kodekutters.dokoni.server;

import com.kodekutters.dokoni.core.Message;

/**
 *
 * @author R. Wathelet, March 2012
 * @version 1.0
 */
public interface MessageConsumer {

    void receiveMessage(Message message);
}
