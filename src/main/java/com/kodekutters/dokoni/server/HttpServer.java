package com.kodekutters.dokoni.server;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.security.Constraint;
import org.openide.util.Exceptions;

/**
 * HttpServer
 *
 * @author Ringo Wathelet
 * @version 1.0, 2013
 */
public class HttpServer {

    protected Server server;
    protected PhoneRequestHandler handler = new PhoneRequestHandler();
    public static int port = 6082;

    public HttpServer() {
//        init();
        initSimple();
    }

    public HttpServer(int port) {
        this.port = port;
//        init();
        initSimple();
    }

    public HttpServer(int port, PhoneRequestHandler handler) {
        this.port = port;
        this.handler = handler;
//        init();
        initSimple();
    }

    private void initSimple() {
        server = new Server(port);
        handler = new PhoneRequestHandler();
        server.setHandler(handler);
    }

    public Server getServer() {
        return server;
    }

    public PhoneRequestHandler getHandler() {
        return handler;
    }

    /**
     * -- TODO
     */
    private void init() {
        server = new Server(this.port);

        LoginService loginService = new HashLoginService("MyRealm", "src/test/resources/realm.properties");
        server.addBean(loginService);

        ConstraintSecurityHandler security = new ConstraintSecurityHandler();
        server.setHandler(security);

        Constraint constraint = new Constraint();
        constraint.setName("auth");
        constraint.setAuthenticate(true);
        constraint.setRoles(new String[]{"user", "admin"});
        ConstraintMapping mapping = new ConstraintMapping();
        mapping.setPathSpec("/*");
        mapping.setConstraint(constraint);

        Set<String> knownRoles = new HashSet<String>();
        knownRoles.add("user");
        knownRoles.add("admin");

        security.setConstraintMappings(Collections.singletonList(mapping), knownRoles);
//        security.setAuthenticator(new SimpleAuthenticator());
        security.setLoginService(loginService);
        security.setStrict(false);

        handler = new PhoneRequestHandler();

        security.setHandler(handler);

        start();
    }

    public boolean isRunning() {
        return getServer().isRunning();
    }

    public void start() {
        try {
            server.start();
            server.join();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public void stop() {
        try {
            server.stop();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
